/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package DAO;
import static DAO.DAO_login.con;
import java.io.File;
import java.util.List;
import model.resident_model;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.view.JasperViewer;
import net.sf.jasperreports.engine.xml.JRXmlDigesterFactory;

/**
 *
 * @author israj
 */
public class DAO_resident {
    final String insert = "INSERT INTO resident (id, name, phone, address) VALUES (?, ?, ?, ?);";
    final String update = "UPDATE resident set name=?, phone=?, address=? where id=? ;";
    final String delete = "DELETE FROM resident where id=? ;";
    final String select = "SELECT * FROM resident;";
    final String cariid = "SELECT * FROM resident where id like ?";
    static Connection con;
    public Statement st;
    
    JasperReport jasperReport;
    JasperDesign jasperDesign;
    JasperPrint jasperPrint;
    Map<String, Object> parham = new HashMap<String, Object>();
    
    public DAO_resident(){
        con=connection.getConnection();
    }
  
    public void Report(resident_model residentModel) {
        try {
            File file = new File("/home/israj/NetBeansProjects/java_se_crud_login_mvc/src/Report/Resident_report.jrxml");
            jasperDesign = JRXmlLoader.load(file);
            parham.clear();
            jasperReport = JasperCompileManager.compileReport(jasperDesign);
            jasperPrint = JasperFillManager.fillReport(jasperReport, parham, con);
            JasperViewer.viewReport(jasperPrint, false);
        } catch (JRException ex) {
            JOptionPane.showMessageDialog(null, ex);
        }
     }
    
    public void New(resident_model residentModel ){
        PreparedStatement statement = null;
        try 
        {
            statement = con.prepareStatement(insert);
            statement.setString(1, residentModel.getId());
            statement.setString(2, residentModel.getName());
            statement.setString(3, residentModel.getPhone());
            statement.setString(4, residentModel.getAddress());
            statement.executeUpdate();
            ResultSet rs = statement.getResultSet();
            JOptionPane.showMessageDialog(null, "Data Saved");
        } 
        catch (SQLException ex) 
        {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Duplicate ID");
        } 
         
        finally 
        {
            try 
            {
                statement.close();
            } catch (SQLException ex) 
            {
                ex.printStackTrace();
            }
        }
        
    }
    
    public void Update(resident_model residentModel){     
      PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(update);
            statement.setString(1, residentModel.getName());
            statement.setString(2, residentModel.getPhone());
            statement.setString(3, residentModel.getAddress());
            statement.setString(4, residentModel.getId());
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Updated");

        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Duplicate ID");
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
   }
    
    public void Delete(String id) {
        PreparedStatement statement = null;
        try {
            statement = con.prepareStatement(delete);

            statement.setString(1, id);
            statement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Data Deleted");
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed Delete Data");
        } finally {
            try {
                statement.close();
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    public List<resident_model> getList() {
        List<resident_model> listResident = null;
        try {
            listResident = new ArrayList<resident_model>();
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(select);
            while (rs.next()) {
                resident_model residentModel = new resident_model();
                residentModel.setId(rs.getString("id"));
                residentModel.setName(rs.getString("name"));
                residentModel.setPhone(rs.getString("phone"));
                residentModel.setAddress(rs.getString("address"));
                listResident.add(residentModel);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO_resident.class.getName()).log(Level.SEVERE, null, ex);
        }

        return listResident;
    }

    public List<resident_model> getCari(String id) {
        List<resident_model> listResident = null;
        try {
            listResident = new ArrayList<resident_model>();
            PreparedStatement st = con.prepareStatement(cariid);
            st.setString(1, "%" + id + "%");
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                resident_model residentModel = new resident_model();
                residentModel.setId(rs.getString("id"));
                residentModel.setName(rs.getString("name"));
                residentModel.setPhone(rs.getString("phone"));
                residentModel.setAddress(rs.getString("address"));
                listResident.add(residentModel);
            }
        } catch (SQLException ex) {
            Logger.getLogger(DAO_resident.class.getName()).log(Level.SEVERE, null, ex);
        }
        return listResident;
    }
    
    
    
}

